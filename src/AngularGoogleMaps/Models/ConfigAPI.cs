﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps.Models
{
    public class ConfigApi : Api
    {
        public ConfigApi()
        {
        }

        public ConfigApi(ConfigApi other)
        {
            GoogleApiKey = other.GoogleApiKey;
            CoordinateSystem = other.CoordinateSystem;
            Search = new Search()
            {
                Status = other.Search.Status,
                Limit = new Limit()
                {
                    Country = other.Search.Limit.Country
                }
            };
        }

        public ConfigApi(string value)
        {
            var other = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigApi>(value);
            GoogleApiKey = other.GoogleApiKey;
            CoordinateSystem = other.CoordinateSystem;
            Search = new Search()
            {
                Status = other.Search.Status,
                Limit = new Limit()
                {
                    Country = other.Search.Limit.Country
                }
            };
        }

        public static implicit operator ConfigApi(string value)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigApi>(value);
        }

        public static implicit operator string(ConfigApi api)
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            return Newtonsoft.Json.JsonConvert.SerializeObject(api, Newtonsoft.Json.Formatting.None, settings);
        }    
        
        public override string ToString()
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolver();
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.None, settings);
        }

    }
}

